﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Praca_domowa_1
{
    class Program
    {

        static void Main(string[] args)
        {
            
            Console.WriteLine("Witaj w Bibliotece naciśnij klawisz 'Enter' aby przejść dalej");
            Console.ReadKey();

            Shelf shelf = new Shelf();
            shelf.Books = new List<Book>();

            while (true)
            {

                Console.WriteLine();

                Console.WriteLine("Dodaj książkę '1' ");

                Console.WriteLine("Znajdź książki po tytule '2' ");

                Console.WriteLine("Znajdź książki po imieniu i nazwisku autora '3' ");

                Console.WriteLine("Wyjdź '4' ");

                Console.WriteLine();


                string opcja = Console.ReadLine();
                int option = Convert.ToInt32(opcja);


                if (option == 1)
                {

                    var book = new Book();
                    Console.WriteLine("Tytuł książki");
                    var tytul = Console.ReadLine();
                    book.Title = tytul;

                    Console.WriteLine("Imię autora książki");
                    var autorN = Console.ReadLine();
                    book.AuthorN = autorN;

                    Console.WriteLine("Nazwisko autora książki");
                    var autorNazw = Console.ReadLine();
                    book.AuthorLn = autorNazw;

                  

                    bool isYearANumber = false;
                    while (!isYearANumber)
                        {
                        Console.WriteLine("Rok wydania książki");
                        var yearFromUser = Console.ReadLine();

                        isYearANumber = int.TryParse(yearFromUser, out int year);
                        if (isYearANumber)
                        {
                            if (year <= DateTime.Now.Year && year >= 0)
                            {
                                book.Year = year;
                            }
                            else
                            {
                                Console.WriteLine($"Podany parametr nie jest poprawnym rokiem. 0 - {DateTime.Now.Year}");
                                isYearANumber = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Podana wartość nie jest liczbą");
                        }
                    }

                    Console.WriteLine("Tytuł cyklu (lub puste, jeśli książka nie jest częścią cyklu)");
                    var cykl = Console.ReadLine();
                    book.Series = cykl;

                    shelf.Books.Add(book);


                    Console.WriteLine();

                    Console.WriteLine("Naciśnij dowolny klaiwsz aby wyjść do menu");

                }
                else if (option == 2)
                {

                    Console.WriteLine("Szukanie książki po tytule");

                    Console.WriteLine("Podaj Tytul");
                    string searchphrase = Console.ReadLine();

                    List<Book> matchedBooks = new List<Book>();
                    List<Book> booksfromseries = new List<Book>();
                    //Przegladamy wszystkie ksiazki
                    foreach (Book book in shelf.Books)
                    {
                        
                        if (book.Title == searchphrase)
                        {
                            matchedBooks.Add(book);

                            
                        }
                    }

                    //Sprawdzamy czy lista zmatchowanych jest pusta
                    if (matchedBooks.Count == 0)
                    {
                        Console.WriteLine("Sorki, nie znalazlem zadnej ksiaki.");
                    }
                    else
                    {
                        int indeks = 1;
                        foreach (Book matchedBook in matchedBooks)
                        {
                            Console.WriteLine($" <{indeks}><{matchedBook.Title}><{matchedBook.AuthorN}>" +
                                $"<{matchedBook.AuthorLn}>{matchedBook.Year}> ");

                            Console.WriteLine($"Inne książki z cyklu:{matchedBook.Series}");
                           

                            foreach (Book bookfromseries in shelf.Books)
                            {
                                if (matchedBook.Series == bookfromseries.Series && matchedBook.Title != bookfromseries.Title)
                                {
                                    Console.WriteLine($"    .{bookfromseries.Title}");
                                }

                            }

                            indeks = indeks + 1;
                            //indeks++;
                        }

                    }
                }
                else if (option == 3)
                {

                    Console.WriteLine("Szukanie książki po autorze");
                    Console.WriteLine();

                    Console.WriteLine("Podaj imię autora:");

                    string imieA = Console.ReadLine();

                    Console.WriteLine("Podaj nazwisko autora:");

                    string nazwiskoA = Console.ReadLine();

                    List<Book> matchedname = new List<Book>();


                    foreach (Book name in shelf.Books)
                    {
                        bool ismatchedname = name.AuthorN == imieA & name.AuthorLn == nazwiskoA;

                        if (ismatchedname == true)
                        {
                            matchedname.Add(name);

                        }

                    }

                    if (matchedname.Count == 0)

                    {
                        Console.WriteLine("Nie znaleziono książki danego autora");

                    }

                    else

                    {
                        int indeks = 1;
                        foreach (Book matchedBook in matchedname)
                        {
                            Console.WriteLine($" <{indeks}><{matchedBook.Title}><{matchedBook.AuthorN}>" +
                                $"<{matchedBook.AuthorLn}>{matchedBook.Year}> ");
                            indeks = indeks + 1;


                        }
                    }

                    }

                else
                {
                    Console.WriteLine("Bye!");
                    break;
                }
                Console.ReadKey();


            }

        }
    }
}
