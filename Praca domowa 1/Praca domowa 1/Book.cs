﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Praca_domowa_1
{
    public class Book
    {
       public string Title { get; set; }
       public string AuthorN { get; set; }
       public string AuthorLn { get; set; }
       public int Year { get; set; }
       public string Series { get; set; }
        
    }
}
