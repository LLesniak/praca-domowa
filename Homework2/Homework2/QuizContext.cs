﻿using System.Data.Entity;

namespace Homework2
{
    public class QuizContext : DbContext
    {

        public QuizContext() : base("name=Quiz")
        {

        }

        public DbSet<Question> Questions { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
