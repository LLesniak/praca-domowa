﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2
{
    class Program
    {
        public static bool ValidateQuestionContents(string questionContents)
        {
            if (questionContents.Length < 10)
            {
                Console.WriteLine("Pytanie powinno zawierać więcej niż 10 znaków");
            }
            else if (questionContents.Length > 100)
            {
                Console.WriteLine("Pytanie powinno zawierać maksymalnie 100 znaków");
            }
            else if (questionContents.Last() != '?')
            {
                Console.WriteLine("Pytanie powinno zakończyć się znakiem \"?\"");
            }
            else
            {
                return true;
            }
            return false;

        }
        public static bool ValidateCategoryName(string categoryName)
        {
            using (var context = new QuizContext())
            {
                int categoryMaxLenght = 3;
                if (categoryName.Length < categoryMaxLenght)
                {
                    Console.WriteLine($"Kategoria powinna mieć minimum {categoryMaxLenght} znaki. \nWpisz Kategorię ponownie");
                }
                else if (context.Categories.Count(x => x.Name == categoryName) > 0)
                {
                    Console.WriteLine("Taka kategoria już istnieje \nWpisz Kategorię ponownie");
                }
                else
                {
                    return true;
                }
                return false;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Witamy w Quizie");
            Console.WriteLine("================");


            while (true)
            {
                Console.WriteLine("(1) Dodaj Kategorie ");
                Console.WriteLine("(2) Dodaj pytanie do kategorii ");
                Console.WriteLine("(3) Wykonaj Test .");
                Console.WriteLine("(9) Wyjdz .");
                int option = Convert.ToInt32(Console.ReadLine());

                if (option == 9)
                {
                    break;
                }

                if (option == 1)
                {
                    AddCategory();
                }

                if (option == 2)
                {
                    AddQuestionsToCategory();
                }

                if (option == 3)
                {
                    PerformTest();
                }
            }
        }

        private static void PerformTest()
        {
            Console.WriteLine("Witaj w Quizie, wybierz kategorię");

            bool isTestPerformed = false;
            while (isTestPerformed == false)
            {
                using (var context = new QuizContext())
                {
                    //1.Podawanie kategorii
                    Console.WriteLine("Podaj Kategorię z wyświetlonych poniżej");
                    foreach (Category category in context.Categories.ToList())
                    {
                        Console.WriteLine(category.Name);
                    }

                    string givenCategoryName = Console.ReadLine();
                    Category givenCategory = context.Categories.FirstOrDefault(x => x.Name == givenCategoryName);
                    if (givenCategory != null)
                    {
                        var question = context.Questions.Where(x => x.Category.Name == givenCategory.Name).ToList().AsRandom().FirstOrDefault();
                        if (question == null)
                        {
                            Console.WriteLine("Nie znalazłem pytań w tej kategorii.");
                        }
                        else
                        {
                            var answers = new List<string>()
                            {
                                question.BadAnswer1,
                                question.BadAnswer2,
                                question.BadAnswer3,
                                question.GoodAnswer
                            };

                            int increment = 1;
                            foreach (var answer in answers)
                            {
                                Console.WriteLine($"[{increment}] " + answer);
                                increment++;
                            }

                            Console.WriteLine("Podaj odpowiedź (1-4).");
                            int option = Int32.Parse(Console.ReadLine());
                            string chosenAnswer = answers[option - 1];

                            if (chosenAnswer.Equals(question.GoodAnswer))
                            {
                                Console.WriteLine("UUUhahaa, jesteś debeściak.");
                            }
                            else
                            {
                                Console.WriteLine("Bardzo się starałeś lecz z gry wyleciałeś, hehehe.");
                            }
                        }
                    }
                }
            }
        }

        private static void AddQuestionsToCategory()
        {
            Question question = new Question();
            //1. Zapytanie o wybor kategorii
            bool isCategoryPicked = false;
            bool answersProvided = false;
            while (isCategoryPicked == false && answersProvided == false)
            {
                using (var context = new QuizContext())
                {
                    //1.Podawanie kategorii
                    Console.WriteLine("Podaj Kategorię z wyświetlonych poniżej");
                    foreach (Category category in context.Categories.ToList())
                    {
                        Console.WriteLine(category.Name);
                    }

                    string givenCategoryName = Console.ReadLine();

                    Category givenCategory = context.Categories.FirstOrDefault(x => x.Name == givenCategoryName);
                    if (givenCategory != null)
                    {
                        //1A. Przypisanie kategorii
                        isCategoryPicked = true;
                        question.Category = givenCategory;

                        while (answersProvided == false)
                        {
                            //2. Wybór pytania
                            bool isQustionProvided = false;
                            while (isQustionProvided == false)
                            {
                                Console.WriteLine("Podaj poprawne pytanie");
                                string givenQuestion = Console.ReadLine();
                                if (ValidateQuestionContents(givenQuestion))
                                {
                                    //2A. Przypisanie pytania
                                    isQustionProvided = true;
                                    question.Contents = givenQuestion;
                                };
                            }

                            //3. Wybór poprawnej odpowiedzi
                            //3A. Przypisanie następuje poprzedzone walidacja (jedna funkcja przykladowo powyzej)
                            Console.WriteLine("Podaj dobrą odpowiedź");
                            string goodAnswer = Console.ReadLine();
                            question.GoodAnswer = goodAnswer;

                            Console.WriteLine("Podaj pierwszą złą odpowiedź");
                            string firstBadAnswer = Console.ReadLine();
                            question.BadAnswer1 = firstBadAnswer;

                            Console.WriteLine("Podaj drugą złą odpowiedź");
                            string secondBadAnswer = Console.ReadLine();
                            question.BadAnswer2 = secondBadAnswer;

                            Console.WriteLine("Podaj trzecią złą odpowiedź");
                            string thirdBadAnswer = Console.ReadLine();
                            question.BadAnswer3 = thirdBadAnswer;

                            //standardowe sprawdzenia, etc. etc.
                            answersProvided = true;
                        }

                        context.Questions.Add(question);
                        context.SaveChanges();
                    }
                }
            }
        }

        private static void AddCategory()
        {
            Category categoryToCreate = new Category();

            bool isCategoryPicked = false;

            while (isCategoryPicked == false)
            {
                Console.WriteLine("Podaj nazwę kategorii");

                string givenCategoryName = Console.ReadLine();
                using (var context = new QuizContext())
                {
                    bool isCategoryNameValid = ValidateCategoryName(givenCategoryName);
                    if (isCategoryNameValid == true)
                    {
                        categoryToCreate.Name = givenCategoryName;
                        isCategoryPicked = true;

                        context.Categories.Add(categoryToCreate);
                        context.SaveChanges();
                    }
                }
            }
        }
    }
}
