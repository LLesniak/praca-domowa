﻿
namespace Homework2
{
    public class Question
    {
        public int Id { get; set; }
        public string Contents { get; set; }
        public string GoodAnswer { get; set; }
        public string BadAnswer1 { get; set; }
        public string BadAnswer2 { get; set; }
        public string BadAnswer3 { get; set; }
        public Category Category { get; set; }
    }
}
